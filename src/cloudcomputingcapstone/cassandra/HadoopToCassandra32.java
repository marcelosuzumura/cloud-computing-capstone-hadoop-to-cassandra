package cloudcomputingcapstone.cassandra;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.Host;
import com.datastax.driver.core.Metadata;
import com.datastax.driver.core.Session;
import com.google.common.collect.Lists;

public class HadoopToCassandra32 {

	private Cluster cluster;
	private Session session;

	public static void main(String[] args) {
		HadoopToCassandra32 client = new HadoopToCassandra32();
		client.connect(args[0]); // connect to cassandra

		String part = args[1];
		boolean recreate = Boolean.valueOf(args[2]);

		String exercise = "32";

		// 3.2 toms_flights_from_2008 (x, y, z, day, month, flight_xy, flight_yz)
		String tableName = "g32";
		List<ColumnDefinition> columns = Lists.newArrayList(new ColumnDefinition("x", "text"),
				new ColumnDefinition("y", "text"), new ColumnDefinition("z", "text"),
				new ColumnDefinition("day", "int"), new ColumnDefinition("month", "int"),
				new ColumnDefinition("flight_xy", "text"), new ColumnDefinition("flight_yz", "text"));
		String[] primaryKeyColumns = new String[] { "x", "y", "z", "day", "month" };

		if (recreate) {
			client.dropKeyspace(tableName); // tries to drop the keyspace if it exists
			client.createKeyspace(tableName); // name_exercise_keyspace
			client.createTable(tableName, columns, primaryKeyColumns);
		}
		client.loadData(exercise, tableName, columns, part); // read from directory e.g.
		// g21-output/part-00000 and inserts on table

		System.out.println("finished exercise " + exercise);

		client.close();
	}

	public Session getSession() {
		return this.session;
	}

	public void connect(String node) {
		this.cluster = Cluster.builder().addContactPoint(node).build();
		Metadata metadata = this.cluster.getMetadata();
		System.out.printf("Connected to cluster: %s\n", metadata.getClusterName());
		for (Host host : metadata.getAllHosts()) {
			System.out.printf("Datatacenter: %s; Host: %s; Rack: %s\n", host.getDatacenter(), host.getAddress(),
					host.getRack());
		}
		this.session = this.cluster.connect();
	}

	public void createKeyspace(String keyspace) {
		this.session.execute("CREATE KEYSPACE IF NOT EXISTS " + keyspace
				+ " WITH replication = {'class' : 'SimpleStrategy', 'replication_factor' : 2};");
		System.out.println("created keyspace: " + keyspace);
	}

	public void createTable(String tableName, List<ColumnDefinition> columns, String[] primaryKeyColumns) {
		StringBuilder tableSB = new StringBuilder();
		tableSB.append("CREATE TABLE IF NOT EXISTS ");
		tableSB.append(tableName);
		tableSB.append(".");
		tableSB.append(tableName);
		tableSB.append("(");
		// tableSB.append("id bigint,");
		for (ColumnDefinition column : columns) {
			tableSB.append(column.name);
			tableSB.append(" ");
			tableSB.append(column.type);
			tableSB.append(",");
		}
		tableSB.append("PRIMARY KEY (");
		// tableSB.append("id");
		for (String column : primaryKeyColumns) {
			tableSB.append(column);
			tableSB.append(",");
		}
		tableSB.deleteCharAt(tableSB.length() - 1);
		tableSB.append("));");

		this.session.execute(tableSB.toString());

		System.out.println("created table: " + tableName + "." + tableName);
	}

	public void loadData(String exercise, String tableName, List<ColumnDefinition> columns, String part) {
		int rows = 1;
		long start = System.currentTimeMillis();

		BufferedReader reader = null;
		try {
			reader = new BufferedReader(
					new FileReader(new File("/home/ubuntu/results/g" + exercise + "-output/part-r-00000-" + part)));

			String line = null;
			while ((line = reader.readLine()) != null) {
				String[] values = this.parse(exercise, line);

				StringBuilder insertSB = new StringBuilder();
				insertSB.append("INSERT INTO ");
				insertSB.append(tableName);
				insertSB.append(".");
				insertSB.append(tableName);
				// insertSB.append("(id,");
				insertSB.append("(");
				int i = 0;
				for (ColumnDefinition column : columns) {
					insertSB.append(column.name);
					if (i < columns.size() - 1) {
						insertSB.append(",");
					}
					i++;
				}
				insertSB.append(") VALUES (");

				i = 0;
				for (ColumnDefinition column : columns) {
					insertSB.append(column.type.equals("text") ? "'" : "");
					insertSB.append(values[i]);
					insertSB.append(column.type.equals("text") ? "'" : "");
					if (i < columns.size() - 1) {
						insertSB.append(",");
					}
					i++;
				}
				insertSB.append(");");

				this.session.execute(insertSB.toString());

				if (rows % 100000 == 0) {
					long now = System.currentTimeMillis();
					// System.out.println((double) (now - start) / 1000 / 60);
					// System.out.println(rows / ((double) (now - start) / 1000 / 60));

					double rowsPerMin = (rows / ((double) (now - start) / 1000 / 60));
					// System.out.println(rowsPerMin);
					double eta = (30000000 - rows) / rowsPerMin;
					// System.out.println(eta);

					System.out.println(rows + " " + " rows left " + (30000000 - rows) + " (" + rowsPerMin
							+ " rows/min) eta (" + eta + " min)");
				}

				rows++;
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	private String[] parse(String exercise, String line) {
		String[] values = null;
		if (exercise.equals("21")) {
			String[] originCarrierPerformance = line.split("\t");
			String[] originCarrier = originCarrierPerformance[0].split(",");
			String origin = originCarrier[0];
			String carrier = originCarrier[1];
			String performance = originCarrierPerformance[1];
			values = new String[] { origin, carrier, performance };

		} else if (exercise.equals("22")) {
			String[] originDestinationPerformance = line.split("\t");
			String[] originDestination = originDestinationPerformance[0].split(",");
			String origin = originDestination[0];
			String destination = originDestination[1];
			String performance = originDestinationPerformance[1];
			values = new String[] { origin, destination, performance };

		} else if (exercise.equals("24")) {
			String[] originDestinationMean = line.split("\t");
			String[] originDestination = originDestinationMean[0].split(",");
			String origin = originDestination[0];
			String destination = originDestination[1];
			String mean = originDestinationMean[1];
			values = new String[] { origin, destination, mean };

		} else if (exercise.equals("32")) {
			String[] keyValues = line.split("\t");

			String[] onlyValues = keyValues[1].split(",");
			String x = onlyValues[0];
			String y = onlyValues[1];
			String z = onlyValues[2];
			String day = onlyValues[3];
			String month = onlyValues[4];
			String flight1 = onlyValues[5];
			String flight2 = onlyValues[6];

			values = new String[] { x, y, z, day, month, flight1, flight2 };
		}

		return values;
	}

	public void close() {
		this.session.close();
		this.cluster.close();
	}

	private void dropKeyspace(String name) {
		this.session.execute("DROP KEYSPACE IF EXISTS " + name + ";");
		System.out.println("dropped keyspace " + name);
	}

	static class ColumnDefinition {

		private String name;
		private String type;

		public ColumnDefinition(String name, String type) {
			this.name = name;
			this.type = type;
		}

	}

}
