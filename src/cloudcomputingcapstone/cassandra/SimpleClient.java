package cloudcomputingcapstone.cassandra;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.Host;
import com.datastax.driver.core.Metadata;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;

public class SimpleClient {

	private Cluster cluster;
	private Session session;

	public Session getSession() {
		return this.session;
	}

	public void connect(String node) {
		this.cluster = Cluster.builder().addContactPoint(node).build();
		Metadata metadata = this.cluster.getMetadata();
		System.out.printf("Connected to cluster: %s\n", metadata.getClusterName());
		for (Host host : metadata.getAllHosts()) {
			System.out.printf("Datatacenter: %s; Host: %s; Rack: %s\n", host.getDatacenter(), host.getAddress(),
					host.getRack());
		}
		this.session = this.cluster.connect();
	}

	public void createSchema() {
		this.session.execute(
				"CREATE KEYSPACE IF NOT EXISTS demo WITH replication = {'class' : 'SimpleStrategy', 'replication_factor' : 1};");

		this.session.execute("CREATE TABLE IF NOT EXISTS demo.users(" + //
				"firstname text," + //
				"lastname text," + //
				"age int," + //
				"email text," + //
				"city text," + //
				"PRIMARY KEY (lastname)" + //
				");");
	}

	public void loadData() {
		// Insert one record into the users table
		this.session.execute(
				"INSERT INTO demo.users (lastname, age, city, email, firstname) VALUES ('Jones', 35, 'Austin', 'bob@example.com', 'Bob')");
		this.session.execute(
				"INSERT INTO demo.users (lastname, age, city, email, firstname) VALUES ('Test', 20, 'Test', 'test@test.com', 'Test')");
	}

	public void querySchema() {
		// Use select to get the user we just entered
		ResultSet results = this.session.execute("SELECT * FROM demo.users WHERE lastname='Jones'");
		for (Row row : results) {
			System.out.format("%s %d\n", row.getString("firstname"), row.getInt("age"));
		}

		System.out.println("-------------------------");

		// Update the same user with a new age
		this.session.execute("update demo.users set age = 36 where lastname = 'Jones'");
		// Select and show the change
		results = this.session.execute("select * from demo.users where lastname='Jones'");
		for (Row row : results) {
			System.out.format("%s %d\n", row.getString("firstname"), row.getInt("age"));
		}

		System.out.println("-------------------------");

		// Delete the user from the users table
		this.session.execute("DELETE FROM demo.users WHERE lastname = 'Jones'");
		// Show that the user is gone
		results = this.session.execute("SELECT * FROM demo.users");
		for (Row row : results) {
			System.out.format("%s %d %s %s %s\n", row.getString("lastname"), row.getInt("age"), row.getString("city"),
					row.getString("email"), row.getString("firstname"));
		}
	}

	public void close() {
		this.session.close();
		this.cluster.close();
	}

	private void dropSchema() {
		// this.session.execute("DROP KEYSPACE IF EXISTS demo;");
	}

	public static void main(String[] args) {
		SimpleClient client = new SimpleClient();
		client.connect("52.72.249.58"); // cassandra1
		client.createSchema();
		client.loadData();
		client.querySchema();
		client.dropSchema();
		client.close();
	}

}
